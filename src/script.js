jQuery(document).ready(function($) {
    $(".headerMenuMobile").on("click", function() {
        $(".headerMenuList").slideToggle();
    });

    $(".headerExtraSearch-icon").on("click", function() {
        $(".headerExtraSearch-fieldWrap").slideToggle();
    });

    $(".mainScreenSlider").slick({
        dots: false,
        appendArrows: $('.mainScreenSlider-nav'),
    });
});
