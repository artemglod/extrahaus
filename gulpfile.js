const gulp  = require("gulp"),
    sass = require("gulp-sass"),
    cleanCSS = require("gulp-clean-css"),
    rename = require("gulp-rename"),
    uglifyes = require("uglify-es"),
    composer = require("gulp-uglify/composer"),
    uglify = composer(uglifyes, console),
    browserSync = require("browser-sync").create();

const compileScss = () =>
    gulp
        .src("src/styles/**/*scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(cleanCSS())
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.stream());

const compressjs = () =>
    gulp
        .src("src/script.js")
        .pipe(uglify())
        .pipe(rename({ extname: ".min.js" }))
        .pipe(gulp.dest("public/js"));

gulp.task("serve", gulp.series(compileScss, compressjs, () => {
    browserSync.init({
        server: "./public"
    });

    gulp.watch("src/styles/**/*.scss", compileScss);
    gulp.watch("src/script.js", compressjs);
    gulp.watch("public/index.html").on("change", browserSync.reload);
}));
